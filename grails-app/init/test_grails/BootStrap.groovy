package test_grails

import com.mbds.itu.g1.Annonce
import com.mbds.itu.g1.Illustration
import com.mbds.itu.g1.Role
import com.mbds.itu.g1.User
import com.mbds.itu.g1.UserRole

class BootStrap {

    def init = { servletContext ->

        def adminRole = new Role(authority: "ROLE_ADMIN").save()
        def moderateurRole = new Role(authority: "ROLE_MODERATEUR").save()

        def adminUser = new User(username: "Gérard", password: "password").save()
        def moderateurUser = new User(username: "Mathilde", password: "password").save()

        UserRole.create(adminUser, adminRole, true)
        UserRole.create(moderateurUser, moderateurRole, true)

        User.list().each {
            User userInstance ->
                (1..5).each {
                    Integer annonceIdx ->
                        def annonceInstance = new Annonce(
                                title: "Titre de l'annonce $annonceIdx",
                                description: "Description de l'annonce $annonceIdx",
                                price: 100 * annonceIdx
                        )
                        (1..5).each {
                            annonceInstance.addToIllustrations(new Illustration(filename: "grails.svg"))
                        }
                        userInstance.addToAnnonces(annonceInstance)

                }
                userInstance.save(flush: true, failOnError: true)
        }

    }

    def destroy = {
    }
}
