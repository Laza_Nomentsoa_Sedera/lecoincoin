package com.mbds.itu.g1

import grails.plugin.springsecurity.annotation.Secured
import org.springframework.web.servlet.ModelAndView

class MenuController {
    static defaultAction = "tableauDeBord"
    def annonceService

    @Secured(['ROLE_ADMIN','ROLE_MODERATEUR'])
    def tableauDeBord() {
        return new ModelAndView("/menu/tableauDeBord")
    }
    @Secured(['ROLE_ADMIN','ROLE_MODERATEUR'])
    def annonce(){
        /*respond annonceService.list(params), model:[pathIllustration: grailsApplication.config.annonces.illustrations.url]*/
        return new ModelAndView("/menu/annonce", [annonces: annonceService.list(), pathIllustration: grailsApplication.config.annonces.illustrations.url])

    }
}
