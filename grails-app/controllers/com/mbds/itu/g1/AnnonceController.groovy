package com.mbds.itu.g1

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import org.springframework.web.servlet.ModelAndView

import static org.springframework.http.HttpStatus.*

class AnnonceController {
    AnnonceService annonceService

    @Secured(['ROLE_ADMIN','ROLE_MODERATEUR'])
    def versModifier(){
        def idAnnonce = params.idAnnonce
        def annonce = annonceService.get(idAnnonce)
        return new ModelAndView("/annonce/versModifier", [actionPage: "Modifier", pathIllustration: grailsApplication.config.annonces.illustrations.url, annonce: annonce])
    }

    @Secured(['ROLE_ADMIN'])
    def versAjouter(){
        return new ModelAndView("/annonce/versModifier", [actionPage: "Ajouter", pathIllustration: grailsApplication.config.annonces.illustrations.url])
    }

    @Secured(['ROLE_ADMIN','ROLE_MODERATEUR'])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond annonceService.list(params), model:[annonceCount: annonceService.count()]
    }
    @Secured(['ROLE_ADMIN','ROLE_MODERATEUR'])
    def show(Long id) {
        respond annonceService.get(id)
    }
    @Secured('ROLE_ADMIN')
    def create() {
        respond new Annonce(params)
    }
    @Secured('ROLE_ADMIN')
    def save(Annonce annonce) {
        if (annonce == null) {
            notFound()
            return
        }

        try {
            annonceService.save(annonce)
        } catch (ValidationException e) {
            respond annonce.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'annonce.label', default: 'Annonce'), annonce.id])
                redirect annonce
            }
            '*' { respond annonce, [status: CREATED] }
        }
    }
    @Secured(['ROLE_ADMIN','ROLE_MODERATEUR'])
    def edit(Long id) {

        respond annonceService.get(id), model: [userList: User.list()/*,pathIllustration: grailsApplication.config.annonces.illustrations.url*/]
    }

    @Secured(['ROLE_ADMIN','ROLE_MODERATEUR'])
    def update() {
        def annonce = Annonce.get(params.id)
        annonce.title = params.title
        annonce.description = params.description
        annonce.price = Double.parseDouble(params.price)
//        annonce.author = User.get(params.author.id)
        if (annonce == null) {
            notFound()
            return
        }
        /**
         * 1. Récupérer le fichier dans la requête
         * 2. Sauvegarder le fichier localement
         * 3. Créer un illustration sur le fichier que vous avez sauvegardé
         * 4. Attacher l'illustration nouvellement créée à l'annonce
         */

        try {
            annonceService.save(annonce)
        } catch (ValidationException e) {
            respond annonce.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'annonce.label', default: 'Annonce'), annonce.id])
                redirect annonce
            }
            '*'{ respond annonce, [status: OK] }
        }
    }
    @Secured('ROLE_ADMIN')
    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        annonceService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'annonce.label', default: 'Annonce'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'annonce.label', default: 'Annonce'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
