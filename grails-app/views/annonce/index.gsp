<!DOCTYPE html>
<html>
<head>


    <meta name="layout" content="main" />

    <g:set var="entityName" value="${message(code: 'annonce.label', default: 'Annonce')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>

<a href="#list-annonce" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/> </a></li>
        <sec:ifAnyGranted roles='ROLE_ADMIN'>
            <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
        </sec:ifAnyGranted>
    </ul>
</div>
<sec:ifAnyGranted roles='ROLE_ADMIN,ROLE_MODERATEUR'>
    <div id="list-annonce" class="content scaffold-list" role="main">
        <h1><g:message code="default.list.label" args="[entityName]" /></h1>
        <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
        </g:if>
        <f:table collection="${annonceList}" />

        <div class="pagination">
            <g:paginate total="${annonceCount ?: 0}" />
        </div>
    </div>
</sec:ifAnyGranted>
</body>
</html>