<%--
  Created by IntelliJ IDEA.
  User: rna
  Date: 2021-03-05
  Time: 20:57
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="baseLayout" />
    <title>Lecoincoin_Modifier une annonce</title>
    <style type="text/css">
        .btnMilieu{
            margin-top: 15px;
            text-align: center;
        }
    </style>
</head>

<body>
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">${actionPage} une annonce</h1>
        </div>
            <div class="row">
                    <div class="col-md-12">
                        <!-- Basic Card Example -->
                        <div class="card shadow mb-12">
                            <div class="card-body">
                                <g:form controller="annonce" action="${actionPage}" method="POST">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div>
                                                <label class="text-xs font-weight-bold text-primary text-uppercase">Titre</label><br/>
                                                <g:if test="${actionPage == 'Ajouter'}">
                                                    <input class="form-control" type="text" name="titre" required="true"/>
                                                </g:if>
                                                <g:else>
                                                    <input class="form-control" type="text" name="titre" required="true" value="${annonce.title}"/>
                                                </g:else>
                                            </div>
                                            <div>
                                                <label class="text-xs font-weight-bold text-primary text-uppercase">Prix ($)</label><br/>
                                                <g:if test="${actionPage == 'Ajouter'}">
                                                    <input class="form-control" type="number" name="prix" required="true"/>
                                                </g:if>
                                                <g:else>
                                                    <input class="form-control" type="number" name="prix" required="true" value="${annonce.price}"/>
                                                </g:else>
                                            </div>
                                            <div>
                                                <label class="text-xs font-weight-bold text-primary text-uppercase">Description</label><br/>
                                                <g:if test="${actionPage == 'Ajouter'}">
                                                    <textarea class="form-control" name="description" rows="4" required="true"></textarea>
                                                </g:if>
                                                <g:else>
                                                    <textarea class="form-control" name="description" rows="4" required="true">${annonce.description}</textarea>
                                                </g:else>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <label class="text-xs font-weight-bold text-primary text-uppercase">Illustrations</label><br/>
                                            <img src = "${pathIllustration}grails.svg"  width="175"/>
                                        </div>
                                    </div>
                                    <div class="btnMilieu">
                                        <g:link controller="menu" action="annonce" class="btn btn-secondary btn-icon-split">
                                            <span class="icon text-gray-600">
                                                <i class="fas fa-arrow-left"></i>
                                            </span>
                                            <span class="text">Annuler</span>
                                        </g:link>
                                        <button class="btn btn-primary btn-icon-split">
                                            <span class="icon text-gray-600">
                                                <i class="fas fa-check"></i>
                                            </span>
                                            <span class="text">Valider</span>
                                        </button>
                                    </div>
                                </g:form>
                            </div>
                        </div>
                    </div>

            </div>

        </div>
        <!-- /.container-fluid -->
    </div>
</body>
</html>