<%--
  Created by IntelliJ IDEA.
  User: rna
  Date: 2021-03-05
  Time: 10:18
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="baseLayout" />
    <title>Lecoincoin_Annonce</title>
    <style type="text/css">
        .aGauche{
            float: left;
        }
    </style>
</head>

<body>
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Liste des annonces</h1>
            <sec:ifAnyGranted roles='ROLE_ADMIN'>
                <div class="aGauche">
                    <g:link controller="annonce" action="versAjouter" class="btn btn-primary btn-icon-split">
                        <span class="icon text-gray-600">
                            <i class="fas fa-plus"></i>
                        </span>
                        <span class="text">Ajouter une annonce</span>
                    </g:link>
                </div>
            </sec:ifAnyGranted>
        </div>

        <div class="row">
        <div class="row">
            <g:each var="${annonce}" in="${annonces}">
                <div class="col-md-12">
                    <!-- Basic Card Example -->
                    <div class="card shadow mb-12">
                        <div class="card-header py-12">
                            <h6 class="m-0 font-weight-bold text-primary">${annonce.title}</h6>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-8">
                                    ${annonce.description}
                                    <br/><br/>
                                    <ul>
                                        <li><strong>Prix:</strong> ${annonce.price} $</li>
                                        <li><strong>Date de l'annonce:</strong> ${annonce.dateCreated}</li>
                                    </ul>
                                    <g:link controller="annonce" action="versModifier" params="${[idAnnonce: annonce.id]}" class="btn btn-primary btn-icon-split">
                                        <span class="icon text-gray-600">
                                            <i class="fas fa-pencil-alt"></i>
                                        </span>
                                        <span class="text">Modifier</span>
                                    </g:link>
                                    <sec:ifAnyGranted roles='ROLE_ADMIN'>
                                        <a href="#" >
                                            <button onclick="return confirm('Voulez-vous vraiment supprimer pour cette annonce?')" class="btn btn-secondary btn-icon-split">
                                                <span class="icon text-gray-600">
                                                    <i class="fas fa-trash"></i>
                                                </span>
                                                <span class="text">Supprimer</span>
                                            </button>
                                        </a>
                                    </sec:ifAnyGranted>
                                </div>
                                <div class="col-md-4">
                                    <img src = "${pathIllustration}grails.svg"  width="175"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                &nbsp;
            </g:each>

        </div>

    </div>
    <!-- /.container-fluid -->
</div>
</body>
</html>